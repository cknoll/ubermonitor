"""
Command line interface for ubermonitor
"""

import argparse
from . import core


def main():

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument(
        "mode",
        choices=["measurement", "mail-report", "stdout-report"],
        help="`measurement`:   take a measurement and save it to data.yml\n"
        "`mail-report`:   send a report mail to account owner\n"
        "`stdout-report`: print the report to stdout",
    )

    parser.add_argument("--debug", help="enable debug mode", action="store_true")

    args = parser.parse_args()
    core.script_main(args)


if __name__ == "__main__":
    main()
