import subprocess


def get_output_of_command(cmd: str, ensure_return0: bool = True, strip: bool = True):
    # use py3.6-compatible version (i.e. capture_output is not available)
    res = subprocess.run(cmd, stdout=subprocess.PIPE)
    if res.returncode != 0 and ensure_return0:
        msg = f"Unexpected returncode ({res.returncode}) of command `{cmd}`."
        raise ValueError(msg)

    result = res.stdout.decode("utf8")
    if strip:
        result = result.strip()

    return result
