"""
Command line interface for autobrowser package
"""

import argparse
import os
import sys
import re
from . import core

from crontab import CronTab

"""
This module handles the installation (and uninstallation) of cron jobs for ubermonitor (to simplify the installation
of the whole service). It is available via commandline e.g. via `python3 -m ubermonitor.setup_cron install`
"""

python_interpreter = sys.executable


def main(args=None):

    # noinspection PyTypeChecker
    if args is None:
        parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

        parser.add_argument(
            "mode",
            choices=["install", "uninstall"],
            help=f"`install`:   install cronjobs for {core.APPNAME}\n"
            f"`uninstall`: uninstall cronjobs for {core.APPNAME}\n",
        )
        args = parser.parse_args()

    if args.mode == "install":
        cmt1 = f"{core.APPNAME}_measurement"
        cmt2 = f"{core.APPNAME}_mail-report"

        n = 0

        with CronTab(user=os.getenv("USER")) as cron:
            if not list(cron.find_comment(cmt1)):
                job1 = cron.new(command=f"{python_interpreter} -m ubermonitor.cli measurement", comment=cmt1)
                job1.hour.every(1)
                job1.minute.on(18)
                job1.env["MAILTO"] = ""

                n += 1

            if not list(cron.find_comment(cmt2)):
                # !! todo handle the case where core.REPORT_INTERVAL != 24
                job2 = cron.new(command=f"{python_interpreter} -m ubermonitor.cli mail-report", comment=cmt2)
                job2.hour.on(12)
                job2.minute.on(33)
                job1.env["MAILTO"] = ""
                n += 1

        print(n, "cron-jobs installed")

    elif args.mode == "uninstall":
        with CronTab(user=os.getenv("USER")) as cron:
            # !! this might fail if APPNAME contians special regex characters (which is unlikely)
            n = cron.remove_all(comment=re.compile(f"{core.APPNAME}_.*"))
        print(n, "cron-jobs removed")
    else:
        raise ValueError(f"Unexpected mode argument from command line: {args.mode}")


if __name__ == "__main__":
    main()
