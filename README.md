[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# Ubermonitor


A simple script for monitoring the cpu usage of an [uberspace](https://uber.space) account.

## Installation and Removal

Note this is currently in early development and only roughly tested 


### Install the service (with normal user privileges) 
```bash 
# install package and 2 associated cron-jobs
pip3 install --user git+https://codeberg.org/cknoll/ubermonitor.git
```

### Uninstall 

```bash
# note: reverse order w.r.t. installation
python3 -m ubermonitor.setup_cron uninstall
pip3 uninstall ubermonitor
```

---

## How it works

The script comes with two cron jobs:

- measurement-job (e.g. every hour),
- report-job (e.g. once a day).

The measurement job writes information about running processes into `~/.local/ubermonitor/data.yml`. Old data deleted after 24h.

The report-job parses `data.yml`, calculates the average cpu usage of each process and creates a sorted list. If the cummulated cpu-usage passes a certain threshold (e.g. 10%) an email is sent to `$USER@uber.space`.

Also, at any time one can get the current report via `python3 -m ubermonitor.cli stdout-report` 

## Relevant documentation:
 
- https://manual.uberspace.de/daemons-cron/
- https://gitlab.com/doctormo/python-crontab
- https://psutil.readthedocs.io/en/latest/

## Attribution

Part of the code is based on this article: <https://www.thepythoncode.com/article/make-process-monitor-python>.
