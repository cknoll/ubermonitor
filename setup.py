#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
from setuptools import setup, find_packages
from setuptools.command.develop import develop
from setuptools.command.install import install


vi = sys.version_info
if vi.major < 3 or (vi.major == 3 and vi.minor < 6):
    msg = "This package requires at least python3.6"
    raise ValueError(msg)

packagename = "ubermonitor"

# consider the path of `setup.py` as root directory:
PROJECTROOT = os.path.dirname(sys.argv[0]) or "."

with open(os.path.join(PROJECTROOT, "src", packagename, "release.py"), encoding="utf8") as pyfile:
    __version__ = pyfile.read().split('__version__ = "', 1)[1].split('"', 1)[0]


with open("requirements.txt") as requirements_file:
    requirements = requirements_file.read()

# add additional requirements for py3.6 which are incompatible with newer versions
if vi.minor == 6:
    with open("requirements_py3.6.txt") as requirements_file:
        requirements += requirements_file.read()


def post_install_hook():
    # this mechanism is taken from https://stackoverflow.com/a/36902139/333403
    sys.path.append(os.path.join(PROJECTROOT, "src"))
    from ubermonitor import setup_cron

    class Container(object):
        pass

    args = Container
    args.mode = "install"
    setup_cron.main(args)


class PostDevelopCommand(develop):
    """Post-installation for development mode."""

    def run(self):
        develop.run(self)
        post_install_hook()


class PostInstallCommand(install):
    """Post-installation for installation mode."""

    def run(self):
        install.run(self)
        post_install_hook()


setup(
    name=packagename,
    version=__version__,
    author="Carsten Knoll",
    author_email="Carsten.Knoll@posteo.de",
    packages=find_packages("src"),
    package_dir={"": "src"},
    url=f"https://codeberg.org/cknoll/{packagename}",
    license="GPLv3",
    description="very simple process monitoring for uberspace",
    long_description="""
    ...
    """,
    install_requires=requirements,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python :: 3",
    ],
    cmdclass={
        "develop": PostDevelopCommand,
        "install": PostInstallCommand,
    },
    entry_points={"console_scripts": [f"{packagename}={packagename}.cli:main"]},
)
